package fr.afpa.serviceDto;

import fr.afpa.entiteDao.AdresseDao;
import fr.afpa.entiteDao.AuthentificationDao;
import fr.afpa.entiteDao.PersonneDao;
import fr.afpa.entiteDao.RoleDao;
import fr.afpa.entiteMetier.Adresse;
import fr.afpa.entiteMetier.Authentification;
import fr.afpa.entiteMetier.Personne;
import fr.afpa.entiteMetier.Role;
import fr.afpa.serviceDao.ServicePersonneDao;

public class ServicePersonneDto {
	
	public boolean enregistrerPersonne(Personne personne, Adresse adresse, Authentification authentification, Role role) {
		
		PersonneDao personneDao = new PersonneDao();
		personneDao.setNom(personne.getNom());
		personneDao.setPrenom(personne.getPrenom());
		personneDao.setEmail(personne.getEmail());
		personneDao.setDateNaissance(personne.getDateNaissance());
		
		AdresseDao adresseDao = new AdresseDao();
		
		adresseDao.setNumero(adresse.getNumero());
		adresseDao.setRue(adresse.getRue());
		adresseDao.setCodePostal(adresse.getCodePostal());
		adresseDao.setVille(adresse.getVille());
		adresseDao.setVille(adresse.getVille());
		
		
		AuthentificationDao authentificationDao = new AuthentificationDao();
		
		authentificationDao.setLogin(authentification.getLogin());
		authentificationDao.setMotDePasse(authentification.getMotDePasse());
		
		RoleDao roleDao = new RoleDao();
		roleDao.setCodeRole(role.getCodeRole());
		roleDao.setIntitule(role.getIntitule());
		return new ServicePersonneDao().enregistrerPersonne(personneDao, adresseDao, authentificationDao, roleDao);
		
	}

}
