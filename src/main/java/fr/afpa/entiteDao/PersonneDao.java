package fr.afpa.entiteDao;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
@Table(name="personne")
public class PersonneDao {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "personne_generator")
	@SequenceGenerator(name = "personne_generator", sequenceName = "personne_seq",allocationSize = 1, initialValue = 2)
	@Column (name="idpersonne", updatable = false, nullable = false )
	private int idPersonne;

	@Column
	private String nom;

	@Column
	private String prenom;

	@Column
	private String email;

	@Column
	private String tel;

	@Column
	private String dateNaissance;

	/*@Column
	@OneToMany(mappedBy = "adresse")
	@OnDelete(action = OnDeleteAction.CASCADE)
	private AdresseDao adresse;*/

	@OneToOne(cascade = { CascadeType.PERSIST })
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JoinColumn(name = "idrole")
	private RoleDao role;

	@ManyToOne(cascade = { CascadeType.PERSIST })
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JoinColumn(name = "idboutique")
	private BoutiqueDao boutique;

	@OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
	@JoinColumn(name="idauthentification")
	@OnDelete(action = OnDeleteAction.CASCADE)
	private AuthentificationDao authentification;
	
	@Column
	@OneToMany(mappedBy="personne")
	 @OnDelete( action = OnDeleteAction.CASCADE )
	private List<AdresseDao> listeAdresse;
	
	

	public int getIdPersonne() {
		return idPersonne;
	}

	public void setIdPersonne(int idPersonne) {
		this.idPersonne = idPersonne;
	}

	public List<AdresseDao> getListeAdresse() {
		return listeAdresse;
	}

	public void setListeAdresse(List<AdresseDao> listeAdresse) {
		this.listeAdresse = listeAdresse;
	}

	public PersonneDao() {
		super();
	}

	public int getId() {
		return idPersonne;
	}

	public void setId(int id) {
		this.idPersonne = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTel() {
		return tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

	public String getDateNaissance() {
		return dateNaissance;
	}

	public void setDateNaissance(String dateNaissance) {
		this.dateNaissance = dateNaissance;
	}

	public RoleDao getRole() {
		return role;
	}

	public void setRole(RoleDao role) {
		this.role = role;
	}

	/*public AdresseDao getAdresse() {
		return adresse;
	}

	public void setAdresse(AdresseDao adresse) {
		this.adresse = adresse;
	}*/

	public AuthentificationDao getAuthentification() {
		return authentification;
	}

	public void setAuthentification(AuthentificationDao authentification) {
		this.authentification = authentification;
	}

	public BoutiqueDao getBoutique() {
		return boutique;
	}

	public void setBoutique(BoutiqueDao boutique) {
		this.boutique = boutique;
	}

}
