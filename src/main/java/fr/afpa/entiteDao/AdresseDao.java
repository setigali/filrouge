package fr.afpa.entiteDao;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
@Table(name = "adresse")
public class AdresseDao {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "adresse_generator")
	@SequenceGenerator(name = "adresse_generator", sequenceName = "adresse_seq", allocationSize = 1, initialValue = 2)
	@Column(name = "idAdresse", updatable = false, nullable = false)
	private Integer idAdresse;

	@Column
	private int numero;

	@Column
	private String rue;

	@Column(name = "complement_adr")
	private String complementAdr;

	@Column
	private String codePostal;

	@Column
	private String ville;

	@Column
	private String pays;

	@ManyToOne(cascade = { CascadeType.PERSIST })
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JoinColumn(name = "idpersonne")
	private PersonneDao personne;

	public AdresseDao() {
		super();
	}

	public int getId() {
		return idAdresse;
	}

	public void setId(int id) {
		this.idAdresse = id;
	}

	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	public String getRue() {
		return rue;
	}

	public void setRue(String rue) {
		this.rue = rue;
	}

	public String getComplementAdr() {
		return complementAdr;
	}

	public void setComplementAdr(String complementAdr) {
		this.complementAdr = complementAdr;
	}

	public String getCodePostal() {
		return codePostal;
	}

	public void setCodePostal(String codePostal) {
		this.codePostal = codePostal;
	}

	public String getVille() {
		return ville;
	}

	public void setVille(String ville) {
		this.ville = ville;
	}

	public String getPays() {
		return pays;
	}

	public void setPays(String pays) {
		this.pays = pays;
	}

}
