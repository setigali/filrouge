package fr.afpa.entiteDao;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
@Table(name="boutique")
public class BoutiqueDao {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "boutique_generator")
	@SequenceGenerator(name = "boutique_generator", sequenceName = "boutique_seq",allocationSize = 1, initialValue = 2)
	@Column (name="idboutique", updatable = false, nullable = false )
	private int idBoutique;

	@Column
	private String nom;

	@Column
	private String codeEntreprise;

	/*@OneToOne(cascade = { CascadeType.PERSIST })
	@JoinColumn(name = "idAdresse")
	private AdresseDao adresse;*/

	@OneToMany(mappedBy = "boutique")
	@OnDelete(action = OnDeleteAction.CASCADE)
	private List<PersonneDao> personne;

	public int getId() {
		return idBoutique;
	}

	public void setId(int id) {
		this.idBoutique = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getCodeEntreprise() {
		return codeEntreprise;
	}

	public void setCodeEntreprise(String codeEntreprise) {
		this.codeEntreprise = codeEntreprise;
	}

	/*public AdresseDao getAdresse() {
		return adresse;
	}

	public void setAdresse(AdresseDao adresse) {
		this.adresse = adresse;
	}*/

}
