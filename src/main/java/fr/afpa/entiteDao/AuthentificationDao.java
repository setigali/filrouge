package fr.afpa.entiteDao;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="authentification")
public class AuthentificationDao {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "authentification_generator")
	@SequenceGenerator(name = "authentification_generator", sequenceName = "authentification_seq",allocationSize = 1, initialValue = 2)
	@Column (name="idauthentification", updatable = false, nullable = false )
	private int idAuthentification;
	
	@Column
	private String login;
	
	@Column
	private String motDePasse;
	
	@OneToOne(mappedBy = "authentification", cascade = { CascadeType.PERSIST })
	private PersonneDao personne;

	public AuthentificationDao() {
		super();
	}

	public int getId() {
		return idAuthentification;
	}

	public void setId(int id) {
		this.idAuthentification = id;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getMotDePasse() {
		return motDePasse;
	}

	public void setMotDePasse(String motDePasse) {
		this.motDePasse = motDePasse;
	}

}
