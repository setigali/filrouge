package fr.afpa.serviceMetier;

import fr.afpa.entiteMetier.Adresse;
import fr.afpa.entiteMetier.Authentification;
import fr.afpa.entiteMetier.Personne;
import fr.afpa.entiteMetier.Role;
import fr.afpa.serviceDto.ServicePersonneDto;

public class EnregistrerPersonne {
	
	
	public boolean enregistrerPersonne(Personne personne, Adresse adresse, Authentification authentification, Role role) {
	
		Personne personneMetier = new Personne();
		ServicePersonneDto servicePersonneDto = new ServicePersonneDto();
		
		return new ServicePersonneDto().enregistrerPersonne(personne, adresse, authentification, role);
	}

}
