package fr.afpa.entiteMetier;

public class Adresse {

	private int id;
	private int numero;
	private String rue;
	private String complementAdr;
	private String codePostal;
	private String ville;
	private String pays;

	public Adresse() {
		super();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	public String getRue() {
		return rue;
	}

	public void setRue(String rue) {
		this.rue = rue;
	}

	public String getComplementAdr() {
		return complementAdr;
	}

	public void setComplementAdr(String complementAdr) {
		this.complementAdr = complementAdr;
	}

	public String getCodePostal() {
		return codePostal;
	}

	public void setCodePostal(String codePostal) {
		this.codePostal = codePostal;
	}

	public String getVille() {
		return ville;
	}

	public void setVille(String ville) {
		this.ville = ville;
	}

	public String getPays() {
		return pays;
	}

	public void setPays(String pays) {
		this.pays = pays;
	}

}
