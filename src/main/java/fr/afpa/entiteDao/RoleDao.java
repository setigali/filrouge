package fr.afpa.entiteDao;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
@Table(name="role")
public class RoleDao {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "role_generator")
	@SequenceGenerator(name = "role_generator", sequenceName = "role_seq",allocationSize = 1, initialValue = 2)
	@Column (name="idrole", updatable = false, nullable = false )
	private int idRole;
	
	
	private String codeRole;
	
	@Column
	private String intitule;

	@OneToMany(mappedBy = "role")
	@OnDelete(action=OnDeleteAction.CASCADE)
	private List<PersonneDao> personne;



	public RoleDao() {
		super();
	}

	public int getId() {
		return idRole;
	}

	public void setId(int id) {
		this.idRole = id;
	}

	public String getCodeRole() {
		return codeRole;
	}

	public void setCodeRole(String codeRole) {
		this.codeRole = codeRole;
	}

	public String getIntitule() {
		return intitule;
	}

	public void setIntitule(String intitule) {
		this.intitule = intitule;
	}


}
