package fr.afpa.serviceDao;

import org.hibernate.Session;
import org.hibernate.Transaction;

import fr.afpa.entiteDao.AdresseDao;
import fr.afpa.entiteDao.AuthentificationDao;
import fr.afpa.entiteDao.PersonneDao;
import fr.afpa.entiteDao.RoleDao;
import fr.afpa.utils.HibernateUtils;

public class ServicePersonneDao {
	
	public boolean enregistrerPersonne(PersonneDao personne, AdresseDao adresse, AuthentificationDao authentification, RoleDao role ) {
		
		boolean retour = false;
		Session session = null;
		Transaction transaction = null;
		try {
			session = HibernateUtils.getSession();
			transaction = session.beginTransaction();

			session.save(adresse);
			session.save(authentification);
			session.save(role);
			session.save(personne);
			transaction.commit();
			retour = true;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			session.close();

		}
		return retour;
		
	}

}
