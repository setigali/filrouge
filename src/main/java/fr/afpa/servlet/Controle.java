package fr.afpa.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.afpa.entiteMetier.Adresse;
import fr.afpa.entiteMetier.Authentification;
import fr.afpa.entiteMetier.Boutique;
import fr.afpa.entiteMetier.Role;

/**
 * Servlet implementation class Controle
 */
@WebServlet("/Controle")
public class Controle extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Controle() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String nom = request.getParameter("nom");
		String prenom = request.getParameter("prenom");
		String email= request.getParameter("email");
		String telephone = request.getParameter("telephone");
		String dateNaissance = request.getParameter("dateNaissance");
		
		Adresse adresse = new Adresse();
		
		int numero = Integer.parseInt(request.getParameter("numeroRue")) ;
		String rue = request.getParameter("rue");
		String complementAdr = request.getParameter("complementAdresse");
		String codePostal = request.getParameter("codePostal");
		String ville = request.getParameter("ville");
		String pays = request.getParameter("pays");
		
		Role role = new Role();
		
		String codeRole = request.getParameter("codeRole");
		String intitule = request.getParameter("intutleRole");
		
		Authentification authentification = new Authentification();
		
		String login = request.getParameter("login");
		String motDePasse = request.getParameter("MDP");
		
		
	}

}
